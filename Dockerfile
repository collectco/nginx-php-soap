FROM debian:jessie
LABEL authors="Prasetyo Wicaksono<pras@alphacode.my>"

ENV LANGUAGE   en_US.UTF-8

RUN echo "deb http://packages.dotdeb.org jessie all" >> /etc/apt/sources.list && \
    gpg --keyserver keys.gnupg.net --recv-key 89DF5277 && gpg -a --export 89DF5277 | apt-key add - && \
    apt-get update -y && \
    DEBIAN_FRONTEND=noninteractive \
    apt-get install --force-yes -y \
    software-properties-common \
    wget && \

    # Add dotdeb GPG Key
    wget https://www.dotdeb.org/dotdeb.gpg && \
    apt-key add dotdeb.gpg && \

    # Install PHP7
    apt-get install --force-yes -y \
    software-properties-common \
    curl \
    nginx \
    git-core \
    php7.0 \
    php7.0-fpm \
    php7.0-cli \
    php7.0-mysql \
    php7.0-sqlite \
    php7.0-pgsql \
    php7.0-mcrypt \
    php7.0-redis \
    php7.0-gd \
    php7.0-curl \
    php7.0-memcached \
    php7.0-apcu \
    php7.0-mbstring \
    php7.0-xml \
    php7.0-intl \
    php7.0-zip unzip \
    php7.0-bcmath \
    php7.0-soap \

    # Install Supervisor
    supervisor && \

    # Remove cache
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* \
           /tmp/* \
           /var/tmp/* && \

    # Configure Nginx and PHP7
    mkdir -p /etc/nginx && \
    mkdir -p /var/run/php-fpm && \
    mkdir -p /var/log/supervisor && \
    mkdir -p /var/www && \
    rm -rf /var/www/* && \
    rm -f /etc/nginx/sites-available/default && \
    chown -R www-data:www-data /var/www && \

    sed -i -e "s/;daemonize\s*=\s*yes/daemonize = no/g" /etc/php/7.0/fpm/php-fpm.conf && \
    echo "clear_env = no" >> /etc/php/7.0/fpm/php-fpm.conf && \
    sed -i "s/user  nginx;/user  www-data/" /etc/nginx/nginx.conf  && \
    sed -i "s/;date.timezone =.*/date.timezone = UTC/" /etc/php/7.0/fpm/php.ini && \
    sed -i "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/" /etc/php/7.0/fpm/php.ini && \
    sed -i "s/;date.timezone =.*/date.timezone = UTC/" /etc/php/7.0/cli/php.ini && \
    sed -i -e "s/;clear_env\s*=\s*no/clear_env = no/g" /etc/php/7.0/fpm/pool.d/www.conf && \
    phpenmod mcrypt && \

    # Install Composer
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php composer-setup.php --install-dir=/usr/bin --filename=composer && \
    php -r "unlink('composer-setup.php');"

ADD conf/nginx-php-supervisor.ini /etc/supervisor.d/nginx-php-supervisor.ini
ADD conf/default.conf /etc/nginx/sites-available/default

EXPOSE 80 443

CMD ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisor.d/nginx-php-supervisor.ini"]
